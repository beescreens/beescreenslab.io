# Official website for BeeScreens

This is the official website for BeeScreens - Be the screen's project.

## BeeScreens

For more information about BeeScreens, please visist [beescreens.gitlab.io](https://beescreens.gitlab.io)
or its repository on [GitLab](https://gitlab.com/beescreens/beescreens).

## Deployment
To deploy this website, install the following tools:

- `ruby`
- `rubygems`
- `ruby-rdoc`

Export the Ruby Gems to your `$PATH`:

```sh
export PATH=/home/[USERNAME]/.gem/ruby/2.5.0/bin:$PATH
```

Clone this repository:

```sh
git clone git@gitlab.com:beescreens/beescreens.gitlab.io.git
```

Install the dependencies:

```sh
cd beescreens.gitlab.io.git/
bundle install
```

Deploy the website:

```sh
bundle exec jekyll serve
```

Access to the website at the address [127.0.0.1:4000](http://127.0.0.1:4000/)
